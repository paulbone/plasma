# Plasma Language
## *a new programming language*

For a general overview, please visit
[http://plasmalang.org/](http://plasmalang.org/)

Source code hosting has now moved to
[github](https://github.com/PaulBone/plasma).

